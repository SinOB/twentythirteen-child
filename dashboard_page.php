<?php
/*
  Template Name: Member Dashboard
 */
?>
<?php
/**
 * Template that displays for the logged in members dashboard only.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen_Child
 * @since Twenty Thirteen Child 1.0
 */
global $bp;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST["innopt_partner_action"]) && $_POST["innopt_partner_action"] === 'delete') {
        $team_id = intval($_POST["innopt_team_id"]);
        innovage_partnership_delete($team_id);
    }
}
$user_id = get_current_user_id();
$stepslink = bp_loggedin_user_domain('/') . 'steps/';
$total_step_count = innovage_pedometer_get_user_steps_total($user_id);

// get count of invites user currently has available
$groups = groups_get_invites_for_user(bp_loggedin_user_id());
$groupsInviteCount = $groups['total'];
$invitelink = bp_loggedin_user_domain('/') . 'groups/invites';

get_header();
?>

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">

        <?php /* The loop */ ?>
        <?php while (have_posts()) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <hr/><div class="dashboard_header">Welcome to the iStep dashboard</div>
                <hr/>



                <div class="dashboard_group_challenge">
                    <h2>Your Challenge:</h2>
                    <?php if (bp_has_groups(array('user_id' => $user_id))) : ?>
                        <?php
                        while (bp_groups()) : bp_the_group();
                            $group_link = bp_get_group_permalink();
                            $challenge_page = innovage_bp_group_challenge_get_header_link(bp_get_group_id());
                            ?>
                            <div class='temp_dashboard_boarder_challenge'>
                                <a href='<?php echo $group_link ?>'><?php bp_group_avatar() ?></a>
                                <div class='dashboard_group_meta'><a href='<?php echo $group_link . $challenge_page ?>'><?php do_action('bp_group_header_meta'); ?></a></div>
                                <span class="dashboard_group_name"><a href='<?php echo $group_link ?>'><?php bp_group_name(); ?></a></span>

                            </div>
                        <?php endwhile; ?>
                        <a href="./groups" class='innovage_button'>View Group Challenges</a>
                    <?php else: ?>
                        <?php if ($groupsInviteCount > 0): ?>
                            <p>You are not yet a member of a group challenge. You currently have <?php echo $groupsInviteCount; ?> invitation(s) to join a group. </p>
                            <a href='<?php echo $invitelink; ?>' title="View invitations I have received" class='innovage_button'>View Invitations</a>
                            <a href="./groups" title="Find or create a group challenge" class='innovage_button'>Find Group Challenge</a>
                        <?php else: ?>
                            <p>You are not yet a member of a group challenge. Please click the button below to find or create a group challenge.</p>
                            <a href="./groups" class='innovage_button'>Find Group Challenge</a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>

                <div id="searchScreen">
                    <a href="<?php echo esc_url(home_url('/')); ?>" class="cancel">x</a>
                    <div style="width:250px; overflow:auto; margin:0px; padding:0px;  background-color:	#FFFFFF;">
                        <iframe frameBorder="0" width="240" height="322" src=http://www.walk4life.info/widgets/walk-search/display?widget_key=d2lkdGg9MjQwJmFtcDtoZWlnaHQ9MzIyJmFtcDtzZWFyY2h0ZXh0PSZhbXA7dGhlbWU9d2hpdGUmYW1wO2ludHJvZHVjdGlvbl90ZXh0PVdhbnQrdG8ra25vdyt3aGVyZSt0byt3YWxrJTNGK0ZpbmQrYSt3YWxrK25lYXIreW91Li4uJmFtcDtuYW1lPVNpbk9CJmFtcDtlbWFpbF9hZGRyZXNzPSZhbXA7d2Vic2l0ZT1pc3RlcC5vcmcudWsmYW1wO2Zvcm1fYnVpbGRfaWQ9Zm9ybS0zYWUxMWFjMzQ0NDkyMmVkYmU3ZjQ5YzFmYzg4MDQwNSZhbXA7Zm9ybV9pZD13YWxrNGxpZmVfd2lkZ2V0X3dhbGtfc2VhcmNoX2J1aWxkZXJfZm9ybSZhbXA7dG9rZW49NTQ1YTMxZjE4Nzk1ODcuMzQ5MzQwMzU=&place= ></iframe>
                        <p class="footer" style="text-align:center; margin:0 0 5px 0; border:none; padding-bottom:0px; background-color:#ffffff;">

                        </p>
                    </div>
                </div>
                <div id="cover">            
                </div>


                <div class="dashboard_bottom_row">
                    <div class="dashboard_your_steps"><h2>Your Steps:</h2>
                        <p>Since joining iStep you have walked <span class="step_count"><?php echo $total_step_count ?></span> steps</p>
                        <a href="<?php echo $stepslink ?>" class='innovage_button'>View/Add Steps</a>
                        <br/><br/>Not sure where to walk? <a href="#searchScreen" >Click here to search for walks.</a>
                    </div>

                    <div class="dashboard_partner">
                        <h2>Your Partner:</h2>
                        <form method="post" id="innopt_make_partnership">
                            <input type="hidden" name = "innopt_team_id" id="innopt_team_id" value="">
                            <input type="hidden" name="innopt_partner_action" id="innopt_partner_action" value="">
                            <input type="hidden" name="innopt_partner_id" id="innopt_partner_id" value="">
                        </form>

                        <?php if (bp_has_groups(array('user_id' => $user_id))) : ?>
                            <?php
                            while (bp_groups()) : bp_the_group();
                                $challenge_type = groups_get_groupmeta(bp_get_group_id(), 'challenge-approach');
                                if (!($challenge_type == 1 || $challenge_type == 2)) {
                                    continue;
                                }
                                ?>
                                <div class='dashboard_partner_entry'>
                                    <?php
                                    $partner_info = innovage_partner_get_group_partner($user_id, bp_get_group_id());
                                    if (isset($partner_info) && !empty($partner_info)) {
                                        ?>
                                        For group 
                                        <span><a href='<?php bp_group_permalink(); ?>'><?php bp_group_name(); ?></a></span>
                                        your partner is
                                        <?php
                                        $partner_id = $partner_info->user_id;
                                        $team_id = $partner_info->team_id;
                                        ?> 
                                        <?php echo bp_core_get_userlink($partner_id); ?>
                                        <?php echo bp_core_fetch_avatar(array('item_id' => $partner_id)); ?>
                                        <br/><p><?php $percentage_complete = innovage_partner_get_dyad_percentage(bp_get_group_id(), $user_id, $partner_id); ?>
                                            Together your dyad has walked <?php echo $percentage_complete ?>% of this challenge.
                                        </p>
                                        <p>
                                            <a href='<?php echo bp_core_get_userlink($partner_id, false, true) . 'steps/'; ?>' class='innovage_button'>Add Steps</a>
                                            <a href='javascript:submitInnopdDeletePartnershipForm(<?php echo $team_id ?>)' class='innovage_button'>Unpartner</a>
                                        </p>

                                    <?php } else { ?>
                                        <p>For group 
                                            <span><a href='<?php bp_group_permalink(); ?>'><?php bp_group_name(); ?></a></span>

                                            you do not yet have a partner. To find a partner please click on the 'Find/Create a Partner' below.</p>
                                        <form name='get_partner_form' id='get_partner_form' method='POST' action='partner'>
                                            <input type='hidden' name='partner_group_id' id='partner_group_id' value=''/>
                                        </form>

                                        <a href='javascript:submitInnovageViewPartnershipForm(<?php bp_group_id(); ?>);' class='innovage_button'>Find/Create Partner</a>
                                    <?php } ?>
                                </div>
                            <?php endwhile; ?> 
                        <?php else: ?>
                            <p>You will need to join a group challenge before you can choose a partner. Please click on the button above to find a group challenge.</p>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="entry-content">
                </div><!-- .entry-content -->

                <footer class="entry-meta">
                </footer><!-- .entry-meta -->
            </article><!-- #post -->

            <?php comments_template(); ?>
        <?php endwhile; ?>





    </div><!-- #content -->

</div><!-- #primary -->
<?php if (is_user_logged_in()) : ?>
    <?php get_sidebar(); ?>
    <?php get_footer(); ?>
<?php endif; ?>
<!--end of dashboard template-->
