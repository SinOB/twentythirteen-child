<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

</div><!-- #main -->
<footer id="colophon" class="site-footer" role="contentinfo">
    <?php get_sidebar('main'); ?>

    <div class="site-info">
        Developed in association with <a href="http://www.innovage.group.shef.ac.uk/">InnovAge</a>
    </div><!-- .site-info -->

    <a href="<?php echo esc_url(home_url('/') . 'contact-us/'); ?>"  id="fixedbutton">Share your experience with us</a>

    <a href='<?php echo esc_url(home_url('/') . 'terms/'); ?>'>Terms & conditions</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href='<?php echo esc_url(home_url('/') . 'faq/'); ?>'>F.A.Q.</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href='<?php echo esc_url(home_url('/') . 'contact-us/'); ?>'>Contact Us</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href='<?php echo esc_url(home_url('/') . 'cookie-policy/'); ?>'>Cookie Policy</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>