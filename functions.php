<?php
/*
 * twentythirteen-child functions 
 */

/** /
 * Redierct all non-logged in users to the custom login page
 * @global type $bp
 */
add_action('wp', 'innovage_lockdown_redirect', 3);
add_action('init', 'innovage_redirect_fogotten_password_confirm');

/** /
 * Redirect user has forgotten their password after confirmation received
 *
 * @global type $pagenow
 */
function innovage_redirect_fogotten_password_confirm() {
    global $pagenow;
    if (!is_user_logged_in()) {
        if ('wp-login.php' == $pagenow) {
            if (isset($_GET['checkemail']) && $_GET['checkemail'] == 'confirm') {
                wp_redirect(get_option('siteurl') . "/login/" . '?checkemail=confirm');
                exit;
            } else if (isset($_GET['redirect_to'])) {
                wp_redirect(get_option('siteurl') . "/login/");
            }
        }
    }
}

/** /
 * Redirect all non logged in users to the custom login page
 *
 * @global type $wp
 * @global type $bp
 * @return type
 */
function innovage_lockdown_redirect() {
    global $wp, $bp;
    if (!is_user_logged_in()) {
        if (bp_is_activation_page() || bp_is_register_page() || is_page_template('login.php') || is_page_template('loggedout_page.php') || ( in_array($GLOBALS['pagenow'], array('wp-login.php')))
        ) {
            return;
        }
        bp_core_redirect(get_option('siteurl') . "/login/");
        exit;
    } else {
        if (is_page_template('login.php')) {
            bp_core_redirect(get_option('siteurl'));
            return;
        }
        // Do not allow people to view site root members page
        if (bp_current_component() == 'members') {
            // Only allow super admin, admin and editor to view members page
            if (!current_user_can('edit_pages')) {
                bp_core_redirect(get_option('siteurl'));
                return;
            }
        }
    }
}

/** /
 * Redirect all users to the dashboard on login
 * 
 * @global type $user
 * @param type $redirect_to
 * @param type $request
 * @param type $user
 * @return type
 */
function innovage_login_redirect($redirect_to, $request, $user) {
    global $user;
    if (isset($user->roles) && is_array($user->roles)) {
        return home_url();
    } else {
        return $redirect_to;
    }
}

add_filter("login_redirect", "innovage_login_redirect", 10, 3);

/**
 * Enqueue scripts and styles for the front end for child theme
 *
 * @since Twenty Thirteen Child
 */
function twentythirteen_child_scripts_styles() {

    // Loads JavaScript file with functionality specific to Twenty Thirteen Child
    wp_enqueue_script('twentythirteen-child-script', get_stylesheet_directory_uri() . '/js/functions.js', array('jquery'), '2014-07-03', true);
}

add_action('wp_enqueue_scripts', 'twentythirteen_child_scripts_styles');

/** /
 * Redirect the logout page to the login page automatically
 */
function innovage_go_home() {
    wp_redirect(home_url());
    exit();
}

add_action('wp_logout', 'innovage_go_home');

/** /
 * Put username in the message body of the activation email
 * In case where admin resends from admin user page the user_id is not available and so
 * will not include the username in the email.
 * 
 * @param type $message
 * @param type $user_id
 * @param type $activate_url
 * @return type
 */
function innovage_buddypress_activation_message($message, $user_id, $activate_url) {
    if (isset($user_id) && !empty($user_id)) {
        $user = get_userdata($user_id);
        return 'Hello ' . $user->user_login . ', ' . $message;
    }

    return $message;
}

add_filter('bp_core_signup_send_validation_email_message', 'innovage_buddypress_activation_message', 10, 3);

/** /
 * Redirect the wp-login icon url to the home page (defaults to wordpress.org if not)
 *
 * @return type
 */
function innovage_logo_url() {
    return home_url();
}

add_filter('login_headerurl', 'innovage_logo_url');

/**
 * Load a special style sheet for the wp-login pages
 */
function innovage_login_stylesheet() {
    wp_enqueue_style('custom-login', get_stylesheet_directory_uri() . '/style-login.css');
}

add_action('login_enqueue_scripts', 'innovage_login_stylesheet');

/** /
 * Add description to the wp-login image on hover
 *
 * @return string
 */
function innovage_login_logo_url_title() {
    return 'iStep - Intergenerational Support To Encourage Physical Activity';
}

add_filter('login_headertitle', 'innovage_login_logo_url_title');

/** /
 * Change the wp-login main wordpress image to an istep image
 */
function innovage_login_logo() {
    ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/istep.png);
            padding-bottom: 30px;
        }
    </style>
    <?php
}

add_action('login_enqueue_scripts', 'innovage_login_logo');

/** /
 * If the year of birth is two long - generate an error
 *
 * @global type $bp
 */
function innovage_validate_user_registration() {
    global $bp;

    $year_of_birth = $_POST['field_2'];

    if (empty($year_of_birth) || $year_of_birth == '' || strlen($year_of_birth) < 4 || !is_numeric($year_of_birth)) {
        $bp->signup->errors['field_2'] = __('This field must be a 4 digit year (e.g. 1985)', 'buddypress');
    }

    if (!isset($_POST['accept_tos']) || empty($_POST['accept_tos']) || $_POST['accept_tos'] != 'agreed') {
        $bp->signup->errors['accept_tos'] = __('Please accept the Terms and Conditions by checking this box', 'buddypress');
    }
    return;
}

add_action('bp_signup_validate', 'innovage_validate_user_registration');

/** /
 * This function makes the logout function check to see if you are really logged in
 * before issuing a logout command. This avoids the error that gets issued if
 * you are already logged out (i.e. in another window or tab).
 * Original code by: Camden Ross
 */
function innovage_smart_logout() {
    if (!is_user_logged_in()) {
        $smart_redirect_to = !empty($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : '/';
        wp_safe_redirect($smart_redirect_to);
        exit();
    } else {
        check_admin_referer('log-out');
        wp_logout();
        $smart_redirect_to = !empty($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : '/';
        wp_safe_redirect($smart_redirect_to);
        exit();
    }
}

add_action('login_form_logout', 'innovage_smart_logout');

/** /
 * Append a terms and conditions checkbox and link to the registration form
 *
 * @global type $bp
 */
function innovage_add_to_registration() {
    global $bp;
    ?>
    <div class="clear"></div>

    <div class="register-section" id="terms-section">

        <?php do_action('bp_accept_tos_errors') ?>
        <script type="text/javascript">
    <!--

            function innovage_openWindow(theURL, winName, features) { //v2.0
                window.open(theURL, winName, features);
            }
            //-->
        </script>

        <label for="accept_tos">I accept the <a href='../terms' onclick="innovage_openWindow('../terms', 'Terms_and_Conditions', 'scrollbars=yes,width=650,height=500');
                    return false;">terms & conditions</a> (required)</label>
        <input id="accept_tos" name="accept_tos" type="checkbox" value="agreed">

        <p class="description"></p>

    </div>
    <?php
}

add_action('bp_before_registration_submit_buttons', 'innovage_add_to_registration', 36);

/** /
 * Email encode shortcode
 * Need a safe way to display our email address.
 * Technically this should be in a separate plugin - not in the theme
 * Credit to C.Bavota at http://bavotasan.com/2012/shortcode-to-encode-email-in-wordpress-posts/
 * 
 * @param type $atts
 * @param type $content
 * @return type
 */
function email_encode_function($atts, $content) {
    return '<a href="' . antispambot("mailto:" . $content) . '">' . antispambot($content) . '</a>';
}

add_shortcode('email', 'email_encode_function');


add_action('bp_profile_header_meta', 'innovage_display_user_year_of_birth');

/** /
 * Display the members year of birth in their profile header if admin 
 * or editor user
 *
 * @return type
 */
function innovage_display_user_year_of_birth() {
    global $bp;

    if (!isset($bp->displayed_user)) {
        return;
    }

    $user = $bp->displayed_user;

    if (!isset($user) || !isset($user->id)) {
        return;
    }

    if (!current_user_can('edit_pages')) {
        return;
    }

    $args = array(
        'field' => 'Year of birth',
        'user_id' => $user->id
    );

    $yob = bp_get_profile_field_data($args);

    if ($yob) {
        echo '<div class="member-profile-fields"><span class="profile-fields"> Year of birth : ' . $yob . '</span></div>';
    }
}

/** /
 * Remove unwanted tabs from the profile and groups pages
 *
 * @global type $bp
 */
function innovage_remove_group_options() {
    global $bp;

    // Remove tabs from profile.
    // Leaving groups in as a nav option for the moment
    // otherwise when delete a group will hit a 404
    //bp_core_remove_nav_item('groups');
    // Only disable notifications for non super admin users
    if (!is_super_admin()) {
        bp_core_remove_nav_item('notifications');
    }
}

add_action('bp_setup_nav', 'innovage_remove_group_options');

// Since Version 4.1, themes should use add_theme_support() in the functions.php
// file in order to support title tag
function theme_title_tag() {
   add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_title_tag' );

if (!function_exists('_wp_render_title_tag')) {

    function theme_slug_render_title() {
        ?>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <?php
    }

    add_action('wp_head', 'theme_slug_render_title');
}

function custom_theme_setup() {
	$defaults = array(
	'default-color'          => '',
	'default-image'          => '',
	'wp-head-callback'       => '_custom_background_cb',
	'admin-head-callback'    => '',
	'admin-preview-callback' => ''
);
add_theme_support( 'custom-background', $defaults );
}
add_action( 'after_setup_theme', 'custom_theme_setup' );

function innovage_istep_child_stylesheet(){
    wp_register_style( 'istep', get_stylesheet_directory_uri().'/istep.css' );
    wp_enqueue_style( 'istep');
}

add_action( 'wp_head', 'innovage_istep_child_stylesheet');
