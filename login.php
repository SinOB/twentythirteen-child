<?php
/*
  Template Name: Login Page
 */
?>
<?php
/**
 * The template for displaying the custom login page
 * 
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen_Child
 * @since Twenty Thirteen 1.0
 */
global $user_ID, $wpdb;

if (!$user_ID) {

    if ($_POST) {
        $username = !empty($_REQUEST['username']) ? sanitize_user($_REQUEST['username']) : null;
        $password = !empty($_REQUEST['password']) ? trim($_REQUEST['password']) : null;
        $remember = !empty($_REQUEST['rememberme']) ? $_REQUEST['rememberme'] : null;

        if ($remember) {
            $remember = "true";
        } else {
            $remember = "false";
        }
        $login_data = array();
        $login_data['user_login'] = $username;
        $login_data['user_password'] = $password;
        $login_data['remember'] = $remember;
        $user_verify = wp_signon($login_data, false);

        if (!is_wp_error($user_verify)) {
            wp_redirect(home_url());
        }
    }

    get_header();
    ?>

    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <?php
            if (isset($user_verify) && is_wp_error($user_verify)) {
                $limit_message = '';
                if (function_exists('limit_login_get_message')) {
                    $limit_message = limit_login_get_message();
                }
                $errors = $user_verify->get_error_messages();
                if (count($errors) > 0) {
                    echo "<div class='login_box_error' id='login-error'>";
                    foreach ($errors as $error) {
                        if ($limit_message == $error) {
                            continue;
                        }
                        echo $error . '<br/>';
                    }
                    if (!empty($limit_message)) {
                        echo $limit_message;
                    }
                    echo '</div>';
                }
            } else if (isset($_GET['checkemail']) && $_GET['checkemail'] == 'confirm') {
                ?>
                <div class="login_box_message" id="login-error">
                    <p>Please check your e-mail for the confirmation link.</p>
                </div>
                <?php
            }
            ?>
            <div class="login_box">
                <div class="login_box_right">
                    <h2><?php _e('Already have an account?', 'buddypress') ?></h2>
                    <form id="wp_login_form" action="" method="post">
                        <p class="login-username">
                            <label for="user_login">Username</label>
                            <input type="text" name="username" id="user_login" class="input" value="" size="20" />
                        </p>
                        <p class="login-password">
                            <label for="user_pass">Password</label>
                            <input type="password" name="password" id="user_pass" class="input" value="" size="20" />
                        </p>

                        <p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember Me</label></p>
                        <p class="login-submit">
                            <input type="submit" id="submitbtn" name="submit" value="Login">
                        </p>
                    </form>

                    <script type="text/javascript">
                        $("#submitbtn").click(function() {

                            $('#result').html('<img src="<?php echo esc_url( get_template_directory_uri() ) ; ?>/images/loader.gif" class="loader" />').fadeIn();
                            var input_data = $('#wp_login_form').serialize();
                            $.ajax({
                                type: "POST",
                                url: "<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>",
                                data: input_data,
                                success: function(msg) {
                                    $('.loader').remove();
                                    $('
                                            < div > ').html(msg).appendTo('div#result').hide().fadeIn('slow');
                                }
                            });
                            return false;
                        });</script>

                    <a href="<?php echo wp_lostpassword_url(); ?>" title="Lost Password">Lost Your Password?</a>
                </div>
                <div class="verticalLine"></div>
                <div class="login_box_left">
                    <h2>New to iStep</h2>
                    <p>Creating an account is free and instant</p>
                    <a href="../registration/" class='innovage_button'>Create an account</a>
                </div>
            </div>

        </div><!-- #content -->
    </div><!-- #primary -->



    <?php if (is_user_logged_in()) : ?>
        <?php get_sidebar(); ?>
    <?php endif; ?>
    <?php
    get_footer();
}
?>
