<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <title><?php wp_title('|', true, 'right'); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="page" class="hfeed site">
            <header id="masthead" class="site-header" role="banner">
                <a class="home-link" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
                    <h1 class="site-title"><?php bloginfo('name'); ?></h1>
                    <h2 class="site-description"><?php bloginfo('description'); ?></h2>
                </a>
                <?php if (is_user_logged_in()) : ?>
                <div id="navbar" class="navbar">
                    <nav id="site-navigation" class="navigation main-navigation" role="navigation">
                        <!-- start #custom nav bar -->
                        <div class="menu-main-container">
                            <ul  class="nav-menu">
                                <li class="menu-item"><a href="<?php echo wp_logout_url(); ?>">Logout</a></li>
                                <li class="menu-item"><a href="<?php echo bp_loggedin_user_domain('/'). "/profile/" ; ?>">Profile</a></li>
                                <?php if (current_user_can('edit_pages')):?>
                                    <li class="menu-item"><a href="<?php  echo get_option('siteurl') . "/route-builder/" ; ?>">Routes</a></li>
                                <?php endif;?>
                                <li class="menu-item"><a href="<?php echo  bp_loggedin_user_domain('/') ; ?>">My Steps</a></li>
                                <li class="menu-item"><a href="<?php echo get_option('siteurl') . "/groups/"; ?>">Groups</a></li>
                                <li class="menu-item"><a href="<?php echo esc_url(home_url('/')); ?>">Home</a></li>
                            </ul>
                        </div>
                        <!--end #custom nav bar-->

                    </nav><!-- #site-navigation -->
                </div><!-- #navbar -->
                <?php endif; ?>
            </header><!-- #masthead -->
            <div id="main" class="site-main">
